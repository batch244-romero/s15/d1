// This is a statement
console.log("Hello, Batch 244!");

console. log( "Hello, World" ) ;

console.
log
(
	"Hello Again"
)

// [SECTION] Variables
// It is used to contain data

// Declaring variables: tells our device that a variable is created and is ready to store data

// Syntax: let/const variablename;

let myVariable;
console.log(myVariable); //result: undefined

// Initializing variables
// Syntax: let/const variableName = value;

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice)

const interest = 3.539;
console.log(interest)

//Reassigning values
// Syntax: variableName = newValue;
productName = "Laptop";
console.log(productName)

let friend = "Kate";
console.log(friend)
friend = "Jane"
console.log(friend)

let friend1 = "Mark"
console.log(friend1)


// This will result to an error: Assignment to a constant variable
// interest = 4.489;
// console.log(interest)


// let/const locat/global scope

let outerVariable = "Hello";

{
	let innerVariable = "Hello again";
}

console.log(outerVariable);
// console.log(innerVariable); //Result: undefined/uncaught


// Multiple Variable Declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

const greeting = "Hi";
console.log(greeting);


// [SECTION] Data Types
	/*
	Strings
		Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
		Strings in Javascript can be written using either a single (') or double (") quote */


let country = 'Philippines';
let province = "Batangas";
console.log(country, province)


// Concantenation
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting2 = "I live in the " + country + ".";
console.log(greeting2);


let message = "John's emloyees went home early.";
console.log(message);

// Numbers
// Integers/Whole Numbers

let headcount = 26;
console.log(headcount);

// Decimal
let grade = 98.7
console.log(grade)

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combing String and Number
console.log("John's grade last quarter is " + grade + ".");

// Bolean - true or false
let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays - used to store multiple values
// Syntax: arrayName = [elementA, elementB, elementC, ...]

let grades = [98, 92, 90, 94];
console.log(grades);


let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
/* Syntax:  let/const objectName = {
			propertyA: value, 
			propertyB: value,
}
*/

let person = {
	fullName: "John Smith",
	age: 32,
	isMarried: true,
	contact: ["0912345678", "09324234223"],
	address: {
				houseNumber: "345",
				city: "Manila",
	}
}

console.log(person);

let myGrades = {
			firstGrading: 98,
			secondGrading: 92,
			thirdGrading: 90,
			fourthGrading: 94,
}
console.log(myGrades);



